    include "ramdat.asm"
    include "header.asm"

start:                          
        move.b  $A10001,d0
        andi.b  #$0F,d0
        beq.b   no_tmss       ;branch if oldest MD/GEN
        move.l  #'SEGA',$A14000 ;satisfy the TMSS        
no_tmss:
		move.w #$2700, sr       ;disable ints
		move.l #$00000000,a7
 		move.w #$100,($A11100)
		move.w #$100,($A11200)  
		bsr clear_regs
		bsr clear_ram
        bsr setup_vdp
		bsr clear_vram
		
		move.l #$40000000,(a3)
		move.w #$0bff,d4
		lea (font),a5
		bsr vram_loop
		
		bsr load_title
        move.w #$2300, sr       ;enable ints	
		bra loop
		
load_title:
		move.l #$58000000,(a3) ;vram 1800
		move.w #$3f8f,d4
		lea (titlescreen),a5
		bsr vram_loop
		
		move.l #$60000002,(a3) ;vram a000
		move.w #$1b,d4
		move.w #$00c0,d2		
		lea (mapdata1_title),a5
		bsr map_loop			
		
		move.l #$40000003,(a3) ;vram c000
		move.w #$1b,d4
		move.w #$20c0,d2
		lea (mapdata2_title),a5
		bsr map_loop	

		move.l #$c0000000,(a3)		
		move.w #$002f,d4
		lea (title_palette),a5
		bsr vram_loop		
		move.b #$01,vblank_ID
		move.b #$01,title_ID
		lea (music1)+40,a6
		move.l a6,vgm_start
		lea (music1),a6
		rts
		
loop:
		move.b #$ff,vb_flag	
		bsr music_driver
vb_wait:
		tst vb_flag
		bne vb_wait
		bra loop
		
setup_vdp:
        lea    $C00004.l,a3     ;VDP control port
	    lea    $C00000.l,a4     ;VDP data port
        lea    (VDPSetupArray).l,a5
        move.w #0018,d4         ;loop counter
VDP_Loop:
        move.w (a5)+,(a3)       ;load setup array
	    nop
        dbf d4,VDP_Loop
        rts  
vram_Loop:
        move.w (a5)+,(a4)
        dbf d4,vram_Loop
        rts  
map_Loop:
		move.w #$0027,d5
superloop:
        move.w (a5)+,d1
		add.w d2,d1
        move.w d1,(a4)
        dbf d5,superloop
		move.w #$0017,d5
emptyloop:		
		move.w #$0000,(a4)
		dbf d5, emptyloop
        dbf d4,map_Loop
        rts  
clear_regs:
		moveq #$00000000,d0
		move.l d0,d1
		move.l d0,d2
		move.l d0,d3
		move.l d0,d4
		move.l d0,d5
		move.l d0,d6
		move.l d0,d7
		move.l d0,a0
		move.l d0,a1
		move.l d0,a2
		move.l d0,a3
		move.l d0,a4
		move.l d0,a5
		move.l d0,a6
		rts
		
clear_ram:
		move.w #$7FF0,d0
		move.l #$FF0000,a0	
clear_ram_loop:
		move.w #$0000,(a0)+
		dbf d0, clear_ram_loop
		rts
clear_vram:
		move.l #$40000000,(a3)
		move.w #$7fff,d4
vram_clear_loop:
		move.w #$0000,(a4)
		dbf d4, vram_clear_loop
		rts		
		
read_controller:                ;d3 will have final controller reading!
		moveq	#0, d3            
	    moveq	#0, d7
	    move.b  #$40, ($A10009) ;Set direction
	    move.b  #$40, ($A10003) ;TH = 1
    	nop
	    nop
	    move.b  ($A10003), d3	;d3.b = X | 1 | C | B | R | L | D | U |
	    andi.b	#$3F, d3		;d3.b = 0 | 0 | C | B | R | L | D | U |
	    move.b	#$0, ($A10003)  ;TH = 0
	    nop
	    nop
	    move.b	($A10003), d7	;d7.b = X | 0 | S | A | 0 | 0 | D | U |
	    andi.b	#$30, d7		;d7.b = 0 | 0 | S | A | 0 | 0 | 0 | 0 |
	    lsl.b	#$2, d7		    ;d7.b = S | A | 0 | 0 | D | U | 0 | 0 |
	    or.b	d7, d3			;d3.b = S | A | C | B | R | L | D | U |
		rts	
		
termtextloop:					;for strings with terminators
		move.b (a5)+,d4	
		cmpi.b #$23,d4			;# (end of text flag)
		beq return
		andi.w #$00ff,d4
		eor.w #$4000,d4
        move.w d4,(a4)		
		bra termtextloop
		
return:
		rts
returnint:
		rte
		
ErrorTrap:        
		move.w #$666,d0
        bra ErrorTrap

HBlank:
       add.w #$01,hblanks
       bsr skew_H
        rte
skew_H:
		cmpi.w #$0078,hblanks
		ble noskew
	   move.w (a1),d1	   
	   add.l #$6,a1
	   lsr.w #$06,d1
	   move.l #$50000003,(a3) ;set vram write at $d000   	  
	   move.w d1,(a4)		  ;write scroll data
	   move.w d1,(a4)
	   rts
noskew:
	   move.l #$50000003,(a3) ;set vram write at $d000   
       move.w #$0000,(a4)
       move.w #$0000,(a4)
	   rts
VBlank:
		move.b #$00,vb_flag	
		lea vblank_table,a0		
		moveq #$00000000,d0
		move.b vblank_ID,d0
        lsl.b #$1,d0		    ;locate the correct table
		sub.w #$02,d0           ;step back a word 
		add.w d0,a0             ;adjust the address register 
		move.w (a0),d0
		move.l d0,a0            ;switch to direct addressing 
		jmp (a0)
		
vblank_table:
 dc.w vb_title
vb_title:
       add.w #$0002, vblanks	
	   cmpi.w #$0200,vblanks
	    bge reset_vb
	   lea (sine),a1
	   move.l a1,d7
	   add.w vblanks,d7
	   move.l d7,a1
	   move.w #$0000,hblanks
		bsr read_controller
		bsr test_input_title
		rte
		
reset_vb:
	   move.w #$0000,vblanks
	   bra vb_title
		
test_input_title:
		move.b d3,d7
		or.b #$bf,d7
		cmpi.b #$bf, d7
		beq select	
		
		move.b d3,d7
		or.b #$fb,d7
		cmpi.b #$fb, d7
		beq titleleft
		
		move.b d3,d7
		or.b #$f7,d7
		cmpi.b #$f7, d7
		beq titleright		
		
		move.b d3,d7
		or.b #$fd,d7
		cmpi.b #$fd, d7
		beq titledown		
		rts
		
select:
		cmpi.b #$01,title_ID
		beq campaign
		cmpi.b #$02,title_ID
		beq online
		cmpi.b #$03,title_ID
		beq lootcrate
		rts
	
lootcrate:
		rts
		
campaign:
		lea (campaigntext1),a5
		move.l #$64800002,(a3); $a480
		bsr termtextloop
		lea (campaigntext2),a5
		move.l #$65000002,(a3); $a500
		bsr termtextloop
		rts
		
online:
		lea (onlinetext1),a5
		move.l #$64800002,(a3); $a480
		bsr termtextloop
		lea (onlinetext2),a5
		move.l #$65000002,(a3); $a500
		bsr termtextloop
		rts
		
cleartext:
		lea (blank),a5
		move.l #$64800002,(a3); $a480
		bsr termtextloop
		lea (blank),a5
		move.l #$65000002,(a3); $a500
		bsr termtextloop
		lea (blank),a5
		move.l #$65800002,(a3); $a580
		bsr termtextloop
		rts
		
titleleft:
		bsr cleartext
		move.l #$c0340000,(a3)
		move.w #$0eee,(a4)
		move.w #$0222,(a4)
		move.w #$0222,(a4)
		move.b #$01,title_ID
		rts	
titleright:
		bsr cleartext
		move.l #$c0340000,(a3)
		move.w #$0222,(a4)
		move.w #$0eee,(a4)
		move.w #$0222,(a4)
		move.b #$02,title_ID
		rts
titledown:
		bsr cleartext
		move.l #$c0340000,(a3)
		move.w #$0222,(a4)
		move.w #$0222,(a4)
		move.w #$0eee,(a4)
		move.b #$03,title_ID
		rts

	include "music_driver_V2.asm"
	include "data.asm"

ROM_End:
              
              